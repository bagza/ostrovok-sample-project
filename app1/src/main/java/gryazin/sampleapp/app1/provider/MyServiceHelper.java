package gryazin.sampleapp.app1.provider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Dima on 03.04.2014.
 */

public class MyServiceHelper {


    private ArrayList<MyServiceCallbackListener> peopleWhoNeedMe = new ArrayList<MyServiceCallbackListener>();
    private SparseArray<Intent> pendingRequests = new SparseArray<Intent>();
    private AtomicInteger IDCounter = new AtomicInteger();
    private Context context;
    private Handler mHandler = new Handler();


    public MyServiceHelper(Context context){
        this.context = context;

    }

    public void addListener(MyServiceCallbackListener newListener){
        peopleWhoNeedMe.add(newListener);
    }
    public void removeListener(MyServiceCallbackListener oldListener){
        peopleWhoNeedMe.remove(oldListener);
    }

    private int createID() {
        return IDCounter.getAndIncrement();
    }

    public boolean isPending(int requestID) {
        return pendingRequests.get(requestID) != null;
    }

    public void killID(int requestID){
        pendingRequests.remove(requestID);
    }

    private int runRequest(int requestID, Intent i){
        pendingRequests.append(requestID, i);
        context.startService(i);
        return requestID;
    }

    public int ostrovokSampleRequest(){
        final int requestID = createID();
        Intent i = createSampleIntent(context, requestID);
        return runRequest(requestID, i);
    }

    public void cancelRequest(int requestID){
        Intent i = new Intent(context, MyService.class);
        i.putExtra(MyService.EXTRA_REQUEST_ID, requestID);
        i.setAction(MyService.ACTION_CANCEL_COMMAND);

        context.startService(i);
        pendingRequests.remove(requestID);
        //i.put
    }

    private Intent createSampleIntent(final Context context, final int requestID){
        Intent i = new Intent(context, MyService.class);
        i.setAction(MyService.ACTION_EXECUTE_COMMAND);
        i.putExtra(MyService.EXTRA_REQUEST_ID, requestID);
        i.putExtra(MyService.EXTRA_STATUS_RECEIVER, new ResultReceiver(mHandler) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                Intent originalIntent = pendingRequests.get(requestID);
                if (isPending(requestID)) {
                    if (resultCode != MyService.RESPONSE_PROGRESS) {

                        pendingRequests.remove(requestID);
                    }
                    for (MyServiceCallbackListener currentListener : peopleWhoNeedMe) {
                        Log.d("SERVICE _HELPER", "Senging feed " + resultCode);
                        if (currentListener != null) {
                            currentListener.onServiceCallback(requestID, originalIntent, resultCode, resultData);
                        }
                    }
                }
            }
        });

        return i;
    }

}
