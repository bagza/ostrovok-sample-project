package gryazin.sampleapp.app1.provider;

import android.app.IntentService;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.JsonReader;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

public class MyService extends Service {

    public static final String ACTION_EXECUTE_COMMAND = "ACTION_EXECUTE_COMMAND";

    public static final String ACTION_CANCEL_COMMAND = "ACTION_CANCEL_COMMAND";

    public static final String EXTRA_REQUEST_ID = "EXTRA_REQUEST_ID";

    public static final String EXTRA_STATUS_RECEIVER = "STATUS_RECEIVER";

    public static final int RESPONSE_SUCCESS = 0;

    public static final int RESPONSE_FAILURE = 1;

    public static final int RESPONSE_PROGRESS = 2;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private ResultReceiver mResultReceiver;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mResultReceiver = intent.getParcelableExtra(EXTRA_STATUS_RECEIVER);
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        Log.d("MyLogs", intent.toString());
        if (ACTION_EXECUTE_COMMAND.equals(intent.getAction())) {
            mServiceHandler.sendMessage(msg);
        }
        if (ACTION_CANCEL_COMMAND.equals(intent.getAction())) {

        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {

            try {
                InputStream httpIn = GetContent();
                if(httpIn == null){//if Http request wasn't successful
                    //Don't know what to do here:(
                }
                else parseJsonStream(httpIn);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Bundle bundle = new Bundle();
                bundle.putString("Exception", e.toString());
                mResultReceiver.send(MyService.RESPONSE_FAILURE, bundle);
            } catch (IOException e) {
                Bundle bundle = new Bundle();
                bundle.putString("Exception", e.toString());
                mResultReceiver.send(MyService.RESPONSE_FAILURE, bundle);
                e.printStackTrace();
            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            mResultReceiver.send(MyService.RESPONSE_SUCCESS, null);
            stopSelf(msg.arg1);
        }

        //Http work is handled here
        private InputStream GetContent() throws IOException{
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(API.Hotels.MAGIC_QUERY_URI);
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if(statusCode == 200){
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                return content;
            }
            else{
            //Wow, wow, easier, man
                return null;
            }
        }

        private void parseJsonStream(InputStream in) throws IOException{
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            try {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals(HotelInfo.HOTELS_ARRAY)) {
                        reader.beginArray();
                        while (reader.hasNext()) {
                            parseHotel(reader);
                        }
                        reader.endArray();
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
            }
            finally{
                reader.close();
            }
        }

        private void parseHotel(JsonReader reader) throws IOException{

            ContentValues cv = new ContentValues();
            Bundle bundle = new Bundle();

            reader.beginObject();
            while(reader.hasNext()){
                String name = reader.nextName();
                if (name.equals(HotelInfo.HOTEL_ADDRESS)) {
                    cv.put(API.Hotels.HOTEL_ADDRESS, reader.nextString());
                } else if (name.equals(HotelInfo.HOTEL_NAME)) {
                    cv.put(API.Hotels.HOTEL_NAME, reader.nextString());
                } else if (name.equals(HotelInfo.HOTEL_THUMB)) {
                    cv.put(API.Hotels.HOTEL_THUMBNAIL_URL, reader.nextString());
                }
                else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            //cv.put(API.Hotels.REST_STATUS, "");
            Log.d("MYSERVICE", "GOT: " + cv.getAsString(API.Hotels.HOTEL_NAME));
            //bundle.putParcelable("cv", cv);
            //mResultReceiver.send(MyService.RESPONSE_PROGRESS, bundle);
            getContentResolver().insert(API.Hotels.CONTENT_URI, cv);

        }
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("MyServiceThread");
        thread.start();
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }
}
