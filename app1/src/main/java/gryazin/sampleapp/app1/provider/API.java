package gryazin.sampleapp.app1.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Dima on 01.04.2014.
 */
public class API {

    /**
     * Public API for the content provider
     */

    public final static class Hotels implements BaseColumns{

        // This class cannot be instantiated
        private Hotels() {}

        public static final int ID_COLUMN = 0;
        public static final int NAME_COLUMN = 1;
        public static final int ADDRESS_COLUMN = 2;
        public static final int THUMB_URL_COLUMN = 3;
        public static final int REST_STATUS_COLUMN = 4;

        public static final String AUTHORITY = "gryazin.sampleapp.provider";
        public static final String HOTEL_PATH = "hotels";
        public static final Uri HOTEL_URI = Uri.parse("content://"
                + AUTHORITY + "/" + HOTEL_PATH);
        public static final Uri CONTENT_URI = HOTEL_URI;

        // MIME dir and item
        public static final String HOTEL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
                + AUTHORITY + "." + HOTEL_PATH;
        public static final String HOTEL_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
                + AUTHORITY + "." + HOTEL_PATH;

        // DB Fields
        public static final String HOTEL_NAME = "name";
        public static final String HOTEL_ADDRESS = "address";
        public static final String HOTEL_THUMBNAIL_URL = "thumb_url";
        public static final String REST_STATUS = "rest_status";

        public static final String MAGIC_QUERY_URI = "https://ostrovok.ru/api/mobile/v1/search.json?"+
                "_auth=1%3Ae510996db451d1fbf3d6a3ee72a2bf736b7738ed" + "&" +
                "adults=2" + "&" +
                "checkin=2014-12-17" + "&" +
                "checkout=2014-12-18" + "&" +
                "limit=50" + "&" +
                "page=1" + "&" +
                "region_id=2395";
   }
}
