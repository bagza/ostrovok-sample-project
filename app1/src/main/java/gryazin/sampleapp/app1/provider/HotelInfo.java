package gryazin.sampleapp.app1.provider;

/**
 * Created by Dima on 03.04.2014.
 */
public class HotelInfo {

    public static final String HOTELS_ARRAY = "hotels";
    public static final String HOTEL_ADDRESS = "address";
    public static final String HOTEL_THUMB = "thumbnail_url";
    public static final String HOTEL_NAME = "name";

}
