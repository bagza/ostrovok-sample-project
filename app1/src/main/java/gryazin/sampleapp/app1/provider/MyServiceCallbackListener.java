package gryazin.sampleapp.app1.provider;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Dima on 03.04.2014.
 */
public interface MyServiceCallbackListener {

    void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle data);

}
