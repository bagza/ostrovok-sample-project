package gryazin.sampleapp.app1.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

public class MyContentProvider extends ContentProvider implements MyServiceCallbackListener {
    public MyContentProvider() {
    }

    private MyServiceHelper serviceHelper;
    private int requestID;
    private boolean updated;
    static final String DB_NAME = "happyisland";
    static final int DB_VERSION  = 1;
    static final String HOTEL_TABLE = "hotels";

    final String LOG_TAG = "myLogs";

    // DB creation command
    static final String DB_CREATE = "create table " + HOTEL_TABLE + "("
                                    + API.Hotels._ID + " integer primary key autoincrement, "
                                    + API.Hotels.HOTEL_NAME + " text, "
                                    + API.Hotels.HOTEL_ADDRESS + " text, "
                                    + API.Hotels.HOTEL_THUMBNAIL_URL + " text, "
                                    + API.Hotels.REST_STATUS + " text"+ ");";

   //// UriMatcher
    private static final int URI_HOTEL = 1;
    private static final int URI_HOTEL_ID = 2;

    private static final UriMatcher sUriMatcher;
    static{
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(API.Hotels.AUTHORITY, API.Hotels.HOTEL_PATH, URI_HOTEL);
        sUriMatcher.addURI(API.Hotels.AUTHORITY, API.Hotels.HOTEL_PATH + "/#", URI_HOTEL_ID);
    }

    DBHelper mDBHelper;
    SQLiteDatabase mDB;

    @Override
    public boolean onCreate() {
        Log.d(LOG_TAG, "onCreate");
        serviceHelper = new MyServiceHelper(getContext());
        serviceHelper.addListener(this);
        requestID = 0;
        updated = true;
        mDBHelper = new DBHelper(getContext());
        mDB = mDBHelper.getWritableDatabase();
        return true;
    }

    /**SQLiteDatabase:
     * query(String table, String[] columns, String selection, String[] selectionArgs,
     * String groupBy, String having, String orderBy, String limit)
     */

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.d(LOG_TAG, "query, " + uri.toString());
        //Handling Uri first
        switch (sUriMatcher.match(uri)) {
            case URI_HOTEL:
                Log.d(LOG_TAG, "URI_HOTEL");
                //Adding default sorting
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = API.Hotels.HOTEL_NAME + " ASC";
                }
                break;
            case URI_HOTEL_ID:
                String id = uri.getLastPathSegment();
                Log.d(LOG_TAG, "URI_HOTEL_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = API.Hotels._ID + " = " + id;
                } else {
                    selection = selection + " AND " + API.Hotels._ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        mDB = mDBHelper.getWritableDatabase();
        //no HAVING and no ORDER BY
        Cursor cursor = mDB.query(HOTEL_TABLE, projection, selection,
                selectionArgs, null, null, sortOrder);
        //Update cursor with every changes in 'hotels' table
        cursor.setNotificationUri(getContext().getContentResolver(),API.Hotels.CONTENT_URI);

        //ASYNC UPDATING
        if((!serviceHelper.isPending(requestID))&!isUpdated()) {
            Log.d(LOG_TAG, "Sending ASYNC REQUEST: " + requestID);
            requestID = serviceHelper.ostrovokSampleRequest();
        }
        return cursor;
    }

    private void verifyValues(ContentValues values){
        if(! values.containsKey(API.Hotels.HOTEL_ADDRESS)) {
            Resources r = Resources.getSystem();
            values.put(API.Hotels.HOTEL_ADDRESS, r.getString(android.R.string.no));
        }
        if(! values.containsKey(API.Hotels.HOTEL_NAME)) {
            Resources r = Resources.getSystem();
            values.put(API.Hotels.HOTEL_ADDRESS, r.getString(android.R.string.unknownName));
        }
        //TODO handle empty thumbnail
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(LOG_TAG, "insert, " + uri.toString());
        verifyValues(values);

        if (sUriMatcher.match(uri) != URI_HOTEL)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        mDB = mDBHelper.getWritableDatabase();

        //Do an update if the constraints match update(HOTEL_TABLE, values, selection, selectionArgs);

        String selection = API.Hotels.HOTEL_NAME + "=?";
        String[] selectionArgs = new String[] {values.getAsString(API.Hotels.HOTEL_NAME)};
        int x = mDB.delete(HOTEL_TABLE, selection, selectionArgs);
        Log.d(LOG_TAG, selection + " " + selectionArgs[0] + " delete="+x);
        long rowID = mDB.insertWithOnConflict(HOTEL_TABLE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        Uri resultUri = ContentUris.withAppendedId(API.Hotels.CONTENT_URI, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count;
        Log.d(LOG_TAG, "delete, " + uri.toString());
        mDB = mDBHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case URI_HOTEL:
                Log.d(LOG_TAG, "URI_HOTEL");
                //To remove all rows and get a count pass "1" as the whereClause.
                count = mDB.delete(HOTEL_TABLE, "1", null);
                break;
            case URI_HOTEL_ID:
                String id = uri.getLastPathSegment();
                Log.d(LOG_TAG, "URI_HOTEL_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = API.Hotels._ID + " = " + id;
                } else {
                    selection = selection + " AND " + API.Hotels._ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        count = mDB.delete(HOTEL_TABLE, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        setUpdated(true);
        return count;
    }

    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count;
        Log.d(LOG_TAG, "update, " + uri.toString());
        switch (sUriMatcher.match(uri)) {
            case URI_HOTEL:
                Log.d(LOG_TAG, "URI_HOTEL");
                setUpdated(false);
                getContext().getContentResolver().notifyChange(uri, null);
                return 0;
            case URI_HOTEL_ID:
                String id = uri.getLastPathSegment();
                Log.d(LOG_TAG, "URI_HOTEL_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = API.Hotels._ID + " = " + id;
                } else {
                    selection = selection + " AND " + API.Hotels._ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        mDB = mDBHelper.getWritableDatabase();
        count = mDB.update(HOTEL_TABLE, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public String getType(Uri uri) {
        Log.d(LOG_TAG, "getType, " + uri.toString());
        switch (sUriMatcher.match(uri)) {
            case URI_HOTEL:
                return API.Hotels.HOTEL_CONTENT_TYPE;
            case URI_HOTEL_ID:
                return API.Hotels.HOTEL_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle data){
        ContentValues cv;
       /* if(data!=null){
            cv = data.getParcelable("cv");
            Log.d(LOG_TAG, "Got service callback: " + cv.getAsString(API.Hotels.HOTEL_NAME));
            this.insert(API.Hotels.HOTEL_URI, cv);
        }
        */
        //TODO if resultCode == Success ??
        switch(resultCode) {
            case MyService.RESPONSE_SUCCESS:
                setUpdated(true);
                break;
            case MyService.RESPONSE_FAILURE:
                setUpdated(true);
                Toast.makeText(getContext(), "Updating failure: " + data.getString("Exception"), Toast.LENGTH_SHORT).show();
        }

    }

    protected void setUpdated(boolean x) {
        updated = x;
    }

    protected boolean isUpdated(){
        return updated;
    }

    protected static class DBHelper extends SQLiteOpenHelper{
        //SQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
        public DBHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(DB_CREATE);
            ContentValues cv = new ContentValues();
            for(int i = 1; i<21; i++){
                cv.put(API.Hotels.HOTEL_NAME, "Hotel " + i);
                cv.put(API.Hotels.HOTEL_ADDRESS, "Address " + i);
                cv.put(API.Hotels.HOTEL_THUMBNAIL_URL, "IMAGE " + i);
                cv.put(API.Hotels.REST_STATUS, "status");
                sqLiteDatabase.insert(HOTEL_TABLE, null, cv);
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
            //no upgrade yet
        }
    }
}
