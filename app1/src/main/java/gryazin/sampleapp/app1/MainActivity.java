package gryazin.sampleapp.app1;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import gryazin.sampleapp.app1.provider.API;

import static gryazin.sampleapp.app1.ItemFragment.OnFragmentInteractionListener;


public class MainActivity extends Activity implements OnFragmentInteractionListener {

    //ItemFragment mWorkFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      /*  FragmentManager fm = getFragmentManager();
        mWorkFragment = (ItemFragment)fm.findFragmentByTag("work");

        if (mWorkFragment == null) {
            mWorkFragment = new ItemFragment();
            fm.beginTransaction().add(mWorkFragment, "work").commit();
        }
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reload:
                reloadSearch();
                return true;
            case R.id.action_discard:
                discardSearch();
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void reloadSearch(){
        getContentResolver().update(API.Hotels.CONTENT_URI, null, null, null);
    }

    private void discardSearch(){
        getContentResolver().delete(API.Hotels.CONTENT_URI, null, null);
    }

    @Override
    public void onFragmentInteraction(String id) {

    }
}
