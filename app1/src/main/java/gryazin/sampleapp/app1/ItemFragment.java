package gryazin.sampleapp.app1;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.ListFragment;
import android.os.Parcelable;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.*;


import gryazin.sampleapp.app1.provider.API;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the
 * interface.
 */
public class ItemFragment extends ListFragment
                          implements LoaderManager.LoaderCallbacks<Cursor>{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private MyCursorAdapter mAdapter;
    private OnFragmentInteractionListener mListener;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    // TODO: Rename and change types of parameters
    public static ItemFragment newInstance(String param1, String param2) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mRequestQueue = Volley.newRequestQueue(getActivity());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache(){
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap){
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url){
                return mCache.get(url);
            }
        });

        //WTF IS THIS??
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        mAdapter = new MyCursorAdapter(getActivity(), null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        /*SimpleCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to, int flags)
        mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.fragment_item, null,
                new String[]{API.Hotels.HOTEL_NAME, API.Hotels.HOTEL_ADDRESS, API.Hotels.HOTEL_THUMBNAIL_URL},
                new int[]{R.id.HotelNameText, R.id.HotelAddressText}, 0);
        */
        setListAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
    }
/*
    public void onActivityCreated(Bundle savedInstanceState) {

        ListView lv = getListView();
        if(lv!=null) lv.onRestoreInstanceState(savedInstanceState.getParcelable("ListState"));
        super.onActivityCreated(savedInstanceState);
    }
*/
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }
/*
    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Save ListView state
        ListView lv = getListView();
        if(lv != null) {
            Parcelable state = lv.onSaveInstanceState();
            outState.putParcelable("ListState", state);
        }
        super.onSaveInstanceState(outState);
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {

        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        //TODO It gets everything. Make something for sorting (see google example)

        //CursorLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
        return new CursorLoader(getActivity(), API.Hotels.CONTENT_URI, null, null, null, API.Hotels._ID + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

    //Providing custom layout for ListFragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, null);
    }

    private class MyCursorAdapter extends CursorAdapter {

        LayoutInflater mInflater;
        ViewHolder holder;

        public MyCursorAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View newView = mInflater.inflate(R.layout.fragment_item, viewGroup, false);
            holder = new ViewHolder();
            holder.hotelAddress = (TextView) newView.findViewById(R.id.hotelAddressText);
            holder.hotelName = (TextView) newView.findViewById(R.id.hotelNameText);
            holder.hotelThumb = (NetworkImageView) newView.findViewById(R.id.hotelThumb);
            newView.setTag(holder);
            return newView;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            holder = (ViewHolder) view.getTag();
            holder.hotelName.setText(cursor.getString(cursor.getColumnIndex(API.Hotels.HOTEL_NAME)));
            holder.hotelAddress.setText(cursor.getString(cursor.getColumnIndex(API.Hotels.HOTEL_ADDRESS)));
            holder.hotelThumb.setImageUrl(cursor.getString(cursor.getColumnIndex(API.Hotels.HOTEL_THUMBNAIL_URL)), mImageLoader);
        }
    }

    static class ViewHolder {
        TextView hotelName;
        TextView hotelAddress;
        NetworkImageView hotelThumb;
        int position;
    }
}
